# mini-project4: Containerize a Rust Actix Web Service
> Xingyu, Zhang (NetID: xz365)

## Setting Up a New Rust Actix Web Project
#### Create a New Project:
```bash
cargo new actix_web_app --bin
cd actix_web_app
```
#### Add Dependencies (in [```Cargo.toml```](./actix_web_app/Cargo.toml)):
```toml
actix-web = "4"
tokio = { version = "1", features = ["full"] }
```
#### Create a Simple Web Server (in [```./src/main.rs```](./actix_web_app/src/main.rs)): 
This simple web server listens on port 8080 and responds to requests at the root URL with _"Hello, Actix!"_
```rust
use actix_web::{web, App, HttpResponse, HttpServer, Responder};

async fn greet() -> impl Responder {
    HttpResponse::Ok().body("Hello, Actix!")
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(greet))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
```
#### Verify The Rust Application Locally: 
Ensure the Rust application compiles and runs correctly on the local machine.
```bash
cargo build --release
```
```bash
cargo run
```
- Terminal Output
![cargo run](./local_verify.png)

- Root URL:
![port 8080](./port.png)

## Setting Up Docker
1. Download [Docker](https://docs.docker.com/get-docker/).
2. Install Docker Following the Official Doc: [Mac](https://docs.docker.com/desktop/install/mac-install/) | [Windows](https://docs.docker.com/desktop/install/windows-install/) | [Linux](https://docs.docker.com/desktop/install/linux-install/) .

## Containerize The Rust Actix Web Service
#### Create a Dockerfile for Your Rust Actix Web Service
1. __Base Image__: Start with ```debian:buster-slim``` as the base image for the first stage of a multi-stage build, labeled as builder. This base image will be used to create a build environment for compiling the Rust application.

2. __Copy Project's Source Code__: Copy the service source code into the Docker image.

3. __Build The Application__: Compile the application using ```cargo build --release```. This command builds the application in release mode, optimizing for speed and size.

4. __Final Image__: Use a lightweight base image for the final stage to keep the size minimal and ensure compatibility between the build and runtime environments, particularly concerning GLIBC versions. Copy the compiled binary from the build stage.

- Complete [```Dockerfile```](./actix_web_app/Dockerfile)
```docker
# Stage 1: Build Environment
FROM debian:buster-slim as builder

# Install necessary packages for building Rust application
RUN apt-get update \
    && apt-get install -y curl build-essential \
    && rm -rf /var/lib/apt/lists/*

# Install Rust
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y

# Add Cargo to PATH
ENV PATH="/root/.cargo/bin:${PATH}"

# Create a working directory
WORKDIR /usr/src/myapp

# Copy your source code
COPY . .

# Build the Rust application
RUN cargo build --release

# Stage 2: Runtime Environment
FROM debian:buster-slim

# Install runtime dependencies, if any
RUN apt-get update \
    && apt-get install -y ca-certificates \
    && rm -rf /var/lib/apt/lists/*

# Copy the binary from the builder stage
COPY --from=builder /usr/src/myapp/target/release/actix_web_app /usr/local/bin/myapp

# Command to run the application
CMD ["myapp"]
```

#### Build the Docker Image
Run the following command in the terminal, in the directory where the Dockerfile is located. (```myapp``` is the _application name_ that defined in previous [```Dockerfile```](./actix_web_app/Dockerfile))
```bash
docker build -t myapp .
```
or __No Cache Build__: Try building the Docker image with the ```--no-cache``` option to ensure it doesn't use the cached layers:
```bash
docker build --no-cache -t myapp .
```

#### Run the Container Locally
```bash
docker run -p 8080:8080 myapp
```
This command maps port 8080 from the container to port 8080 on local host, allowing accessing to the web service from the local machine.

- Terminal Output:
![docker run](./cmd.png)
- Docker Desktop State:
![docker](./docker.png)
- Root URL:
![port](./port.png)
